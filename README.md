<.NOM DU PROJET.>
===============

*date de création* : YYYY/MM/JJ


Destinataires du projet
-----------------------
<.POUR QUI IL EST FAIT.>


Ce que le projet fait
---------------------
<.DESCRIPTION.>


Prérequis
---------
*Ou voir le fichier [INSTALL](INSTALL)*
### Matériels
<.PLATEFORME NÉCÉSSAIRE.>


### Logiciel
<...>


Documentation
-------------

1. [CHANGELOG](CHANGELOG.md) Changements importants d'une version à l'autre
2. [CONTRIBUTING](CONTRIBUTING.md) indications utiles pour les personnes qui voudraient contribuer au projet
3. [CREDITS](CREDITS) liste des contributeurs du projet
4. [FAQ](FAQ.md) "Foire Aux Questions" pour le projet, au format texte
5. [HISTORY](HISTORY.md) histoire du projet
6. [INSTALL](INSTALL.md) instructions de configuration, de compilation et d'installation
7. [LICENSE](LICENSE) termes de la licence
8. [MANIFEST](MANIFEST) liste des fichiers
9. [NEWS](NEWS) dernières nouvelles
10. [TAGS](TAGS) fichier de tags généré automatiquement, pour être utilisé par Emacs ou vi


### Documentation généraliste
#### Git


#### GitFlow


#### LaTex


